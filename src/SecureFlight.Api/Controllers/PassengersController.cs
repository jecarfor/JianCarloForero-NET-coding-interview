﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PassengersController : ControllerBase
    {
        private readonly IService<Passenger> _personService;
        private IRepository<PassengerFlight> _passengerFlightRepository;
        private IRepository<Flight> _flightRepository;

        public PassengersController(IService<Passenger> personService, IRepository<PassengerFlight> passengerFlightRepository, IRepository<Flight> flightRepository)
        {
            _personService = personService;
            _flightRepository = flightRepository;
            _passengerFlightRepository = passengerFlightRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var passengers = (await _personService.GetAllAsync()).Result
                .Select(x => new PassengerDataTransferObject
                {
                    Email = x.Email,
                    FirstName = x.FirstName,
                    Id = x.Id,
                    LastName = x.LastName
                });

            return Ok(passengers);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Add(int flightId, string passengerId)
        {

            _passengerFlightRepository.Add(new PassengerFlight()
            {
                FlightId = flightId,
                PassengerId = passengerId
            });

            var flight = (await _flightRepository.GetAllAsync()).Where(f => f.Id == flightId);

            return Ok(flight);
        }
    }
}
